import javafx.animation.*;
import javafx.application.Application;
import javafx.application.Preloader;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.ToggleButton;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.shape.Polygon;
import javafx.scene.shape.Shape;
import javafx.stage.Stage;
import javafx.util.Duration;

import java.util.ArrayList;
import java.util.List;

import static javafx.util.Duration.millis;

public class Zadanie2 extends Application {
    public void start(Stage stage) {

        //radziecka gwiazda
        Polygon polygon = new Polygon(300, 200, 271, 260, 205, 269, 252, 315, 241, 381, 300, 350, 359, 381, 348, 315, 395, 269, 329, 260); //punkt 00
        polygon.setStrokeWidth(5);
        polygon.setTranslateX(-30); //z lenistwa
        //kolor
        polygon.setFill(Color.RED);

        //tworzenie listy transformacji
        List<Transition> transitions = new ArrayList<>();

        transitions.add(createSequentialTransition(polygon));



        //podzial ekranu
        BorderPane borderPane = new BorderPane();
        borderPane.setStyle("-fx-background-color: transparent;"); //zeby borderPane byl przezroczysty

        final VBox vBox = new VBox();
        vBox.setStyle("-fx-background-color: #dededa;");

        Pane pane = new Pane();
        pane.getChildren().add(polygon);

        borderPane.setCenter(pane);
        borderPane.setLeft(vBox);

        //OBSLUGA PRZYCISKU
        transitions.forEach(transition -> {
            ToggleButton button = new ToggleButton("Start/Stop"); //nazwa przy twprzeniu przycisku
            button.setUserData(transition);
            button.setOnAction(event -> {
                ToggleButton tb = (ToggleButton) event.getSource();
                Transition transite = (Transition) tb.getUserData();
                if (tb.isSelected()) {
                    transite.play();
                } else {
                    transite.stop();
//                    transite.pause();
                }

            });
            vBox.getChildren().add(button);
            button.setMaxWidth(Double.POSITIVE_INFINITY);
        });


        borderPane.setLeft(vBox);

        //koniec podzial ekranu


        Scene scene = new Scene(borderPane, 600, 600, Color.BLACK);
        stage.setScene(scene);
        stage.show();

    }

    public static void main(String[] args) {
        launch(args);
    }

    private Transition createPierwszyKrok(Node node) {
        TranslateTransition st = new TranslateTransition();
//        st.setAutoReverse(true);
//        st.setCycleCount(Timeline.INDEFINITE);
        st.setDuration(millis(2000));
        st.setNode(node);
        st.setFromX(-30);
        st.setFromY(0);
        st.setToX(-30);
        st.setToY(-200);
        return st;
    }

    private Transition createDrugiKrok(Node node) {
        TranslateTransition st2 = new TranslateTransition();
//        st.setAutoReverse(true);
//        st.setCycleCount(Timeline.INDEFINITE);
        st2.setDuration(millis(2000));
        st2.setNode(node);
        st2.setFromX(-30);
        st2.setFromY(-200);
        st2.setToX(120);
        st2.setToY(0);
        return st2;
    }

    private Transition createTrzeciKrok(Node node) {
        TranslateTransition st3 = new TranslateTransition();
//        st.setAutoReverse(true);
//        st.setCycleCount(Timeline.INDEFINITE);
        st3.setDuration(millis(2000));
        st3.setNode(node);
        st3.setFromX(120);
        st3.setFromY(0);
        st3.setToX(-30);
        st3.setToY(210);
        return st3;
    }

    private Transition createCzwartyKrok(Node node) {
        TranslateTransition st4 = new TranslateTransition();
//        st.setAutoReverse(true);
//        st.setCycleCount(Timeline.INDEFINITE);
        st4.setDuration(millis(2000));
        st4.setNode(node);
        st4.setFromX(-30);
        st4.setFromY(210);
        st4.setToX(-200);
        st4.setToY(0);
        return st4;
    }

    private Transition createPiatyKrok(Node node) {
        TranslateTransition st5 = new TranslateTransition();
//        st.setAutoReverse(true);
//        st.setCycleCount(Timeline.INDEFINITE);
        st5.setDuration(millis(2000));
        st5.setNode(node);
        st5.setFromX(-200);
        st5.setFromY(0);
        st5.setToX(-30);
        st5.setToY(0);
        return st5;
    }

    private Transition createZmianaKoloru(Shape shape) { //zmiana wypelnieni nie dziala na wezly
        FillTransition ft = new FillTransition();
        ft.setFromValue(Color.RED);
        ft.setToValue(Color.BLUE);
        ft.setShape(shape);
//        ft.setCycleCount(Timeline.INDEFINITE);
//        ft.setAutoReverse(true);
        ft.setDuration(Duration.millis(2000));
        return ft;
    }

    private Transition createZanikanie(Node node) {
        FadeTransition ft = new FadeTransition(millis(2000), node); //czas i wezeł na ktorym ma dzialac
        ft.setFromValue(1.0);
        ft.setToValue(0.1);
//        ft.setCycleCount(Timeline.INDEFINITE); //ilosc cykli
//        ft.setAutoReverse(true); //autorestart
        return ft;
    }

    private Transition createPojawianie(Node node) {
        FadeTransition ft = new FadeTransition(millis(2000), node); //czas i wezeł na ktorym ma dzialac
        ft.setFromValue(0.1);
        ft.setToValue(1.0);
//        ft.setCycleCount(Timeline.INDEFINITE); //ilosc cykli
//        ft.setAutoReverse(true); //autorestart
        return ft;
    }

    private Transition createObrot(Node node) {
        RotateTransition rt = new RotateTransition();
        rt.setAutoReverse(true);
        rt.setFromAngle(0);
        rt.setToAngle(360);
        rt.setDuration(millis(2000));
        rt.setNode(node);
//        rt.setCycleCount(Timeline.INDEFINITE); blokuje dalsze kroki
        return rt;
    }

    private Transition createParallelPrzesuniecieObrot(Shape node) { //transformacja rownolegla
        ParallelTransition pt = new ParallelTransition();
        pt.getChildren().add(createDrugiKrok(node));
        pt.getChildren().add(createObrot(node));
        return pt;
    }
    private Transition createParallelPrzesuniecieZanikanie(Shape node) { //transformacja rownolegla
        ParallelTransition pz = new ParallelTransition();
        pz.getChildren().add(createTrzeciKrok(node));
        pz.getChildren().add(createZanikanie(node));
        return pz;
    }

    private Transition createParallelPrzesunieciePojawianie(Shape node) { //transformacja rownolegla
        ParallelTransition pp = new ParallelTransition();
        pp.getChildren().add(createCzwartyKrok(node));
        pp.getChildren().add(createPojawianie(node));
        return pp;
    }
    private Transition createParallePrzesuniecieKolor(Shape node){
        ParallelTransition pk = new ParallelTransition();
        pk.getChildren().add(createPiatyKrok(node));
        pk.getChildren().add(createZmianaKoloru(node));
        return pk;
    }

    private Transition createSequentialTransition(Shape node) { //sekwencyjne pausa dziala
        SequentialTransition st = new SequentialTransition();
        st.getChildren().addAll(createPierwszyKrok(node),
                createParallelPrzesuniecieObrot(node),
                createParallelPrzesuniecieZanikanie(node),
                createParallelPrzesunieciePojawianie(node),
                createParallePrzesuniecieKolor(node));

        st.setCycleCount(Timeline.INDEFINITE);
        return st;
    }

}
